from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class AccountListView(TemplateView):
    template_name = 'service/account_list.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context