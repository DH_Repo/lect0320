from django.db import models

# Create your models here.
class LC_Account(models.Model):
    name = models.CharField(max_length=20, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=50, null=True, blank=True)
    profileImg = models.ImageField(null=True, blank=True)
    user = models.ForeignKey('auth.User', blank=True, null=True, related_name='user_account',
                             on_delete=models.CASCADE)
    class Meta:
        db_table = 'account'